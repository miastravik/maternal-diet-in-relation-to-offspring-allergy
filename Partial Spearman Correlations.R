#######################################################################################################
#ADJUSTED SPEARMAN CORRELATIONS
#######################################################################################################
#AllergyMQ1 <- Data on diet during pregnancy (GW 34)
#AllergyMQ2 <- Data on diet during lactation (1 month)
#AllergyMQ3 <- Data on diet during lactation (4 months)

#######################################################################################################
#NAMING FOOD ITEMS FOR VISUALIZATION
#######################################################################################################

MQ1 <- cbind("Grain, low fiber"=AllergyMQ1$Q1_grainnofiber_gday, "Grain, fiber"=AllergyMQ1$Q1_grainfiber_gday, "Bread, total"=AllergyMQ1$Q1_total_bread_gday,
               "Potato"=AllergyMQ1$Q1_potatoall_gday, "Cow's milk"=AllergyMQ1$Q1_milk_gday, "Yoghurt"=AllergyMQ1$Q1_yoghurt_gday, "Cheese"=AllergyMQ1$Q1_cheese_total_gday, 
               "Dairy products"=AllergyMQ1$Q1_dairy_products_gday, "Egg"=AllergyMQ1$Q1_egg_gday,
               "Poultry"=AllergyMQ1$Q1_chicken_gday, "Red meat"=AllergyMQ1$Q1_red_meat_new_gday, "Game meat"=AllergyMQ1$Q1_game_gday,
               "Processed meat"=AllergyMQ1$Q1_processed_meat_new_gday,
               "Offal"=AllergyMQ1$Q1_offal_gday, "Meat, total"=AllergyMQ1$Q1_total_meat_new_gday, "Fatty fish"=AllergyMQ1$Q1_fatty_fish_gday,
               "Lean fish"=AllergyMQ1$Q1_lean_fish_gday,
               "Shellfish"=AllergyMQ1$Q1_seafood_gday, "Seafood, total"=AllergyMQ1$Q1_total_fish_gday, "Vegetables"=AllergyMQ1$Q1_vegetables_gday,
               "Root vegetables"=AllergyMQ1$Q1_rootvegetables_gday,
               "Fruit & berries"=AllergyMQ1$Q1_fruitberries_new_gday, "Vegetarian dishes"=AllergyMQ1$Q1_veg_dish_gday, "Nuts & seeds"=AllergyMQ1$Q1_nutseeds_gday,
               "Soft drinks"=AllergyMQ1$Q1_softdrink_gday, "Pizza"=AllergyMQ1$Q1_pizza_gday, "Sweets"=AllergyMQ1$Q1_sweets_gday, "Snacks"=AllergyMQ1$Q1_snacks_gday)

MQ2 <- cbind("Grain, low fiber"=AllergyMQ2$Q2_grainnofiber_gday, "Grain, fiber"=AllergyMQ2$Q2_grainfiber_gday, "Bread, total"=AllergyMQ2$Q2_total_bread_gday,
               "Potato"=AllergyMQ2$Q2_potatoall_gday, "Cow's milk"=AllergyMQ2$Q2_milk_gday, "Yoghurt"=AllergyMQ2$Q2_yoghurt_gday, "Cheese"=AllergyMQ2$Q2_cheese_total_gday, 
               "Dairy products"=AllergyMQ2$Q2_dairy_products_gday, "Egg"=AllergyMQ2$Q2_egg_gday,
               "Poultry"=AllergyMQ2$Q2_chicken_gday, "Red meat"=AllergyMQ2$Q2_red_meat_new_gday, "Game meat"=AllergyMQ2$Q2_game_gday,
               "Processed meat"=AllergyMQ2$Q2_processed_meat_new_gday,
               "Offal"=AllergyMQ2$Q2_offal_gday, "Meat, total"=AllergyMQ2$Q2_total_meat_new_gday, "Fatty fish"=AllergyMQ2$Q2_fatty_fish_gday,
               "Lean fish"=AllergyMQ2$Q2_lean_fish_gday,
               "Shellfish"=AllergyMQ2$Q2_seafood_gday, "Seafood, total"=AllergyMQ2$Q2_total_fish_gday, "Vegetables"=AllergyMQ2$Q2_vegetables_gday,
               "Root vegetables"=AllergyMQ2$Q2_rootvegetables_gday,
               "Fruit & berries"=AllergyMQ2$Q2_fruitberries_new_gday, "Vegetarian dishes"=AllergyMQ2$Q2_veg_dish_gday, "Nuts & seeds"=AllergyMQ2$Q2_nutseeds_gday,
               "Soft drinks"=AllergyMQ2$Q2_softdrink_gday, "Pizza"=AllergyMQ2$Q2_pizza_gday, "Sweets"=AllergyMQ2$Q2_sweets_gday, "Snacks"=AllergyMQ2$Q2_snacks_gday)

MQ3 <- cbind("Grain, low fiber"=AllergyMQ3$Q3_grainnofiber_gday, "Grain, fiber"=AllergyMQ3$Q3_grainfiber_gday, "Bread, total"=AllergyMQ3$Q3_total_bread_gday,
               "Potato"=AllergyMQ3$Q3_potatoall_gday, "Cow's milk"=AllergyMQ3$Q3_milk_gday, "Yoghurt"=AllergyMQ3$Q3_yoghurt_gday, "Cheese"=AllergyMQ3$Q3_cheese_total_gday, 
               "Dairy products"=AllergyMQ3$Q3_dairy_products_gday, "Egg"=AllergyMQ3$Q3_egg_gday,
               "Poultry"=AllergyMQ3$Q3_chicken_gday, "Red meat"=AllergyMQ3$Q3_red_meat_new_gday, "Game meat"=AllergyMQ3$Q3_game_gday,
               "Processed meat"=AllergyMQ3$Q3_processed_meat_new_gday,
               "Offal"=AllergyMQ3$Q3_offal_gday, "Meat, total"=AllergyMQ3$Q3_total_meat_new_gday, "Fatty fish"=AllergyMQ3$Q3_fatty_fish_gday,
               "Lean fish"=AllergyMQ3$Q3_lean_fish_gday,
               "Shellfish"=AllergyMQ3$Q3_seafood_gday, "Seafood, total"=AllergyMQ3$Q3_total_fish_gday, "Vegetables"=AllergyMQ3$Q3_vegetables_gday,
               "Root vegetables"=AllergyMQ3$Q3_rootvegetables_gday,
               "Fruit & berries"=AllergyMQ3$Q3_fruitberries_new_gday, "Vegetarian dishes"=AllergyMQ3$Q3_veg_dish_gday, "Nuts & seeds"=AllergyMQ3$Q3_nutseeds_gday,
               "Soft drinks"=AllergyMQ3$Q3_softdrink_gday, "Pizza"=AllergyMQ3$Q3_pizza_gday, "Sweets"=AllergyMQ3$Q3_sweets_gday, "Snacks"=AllergyMQ3$Q3_snacks_gday)
############################################################################################

library(MASS)
library(ppcor)

#######################################################################################################
#CONFOUNDER-ADJUSTED SPEARMAN CORRELATIONS BETWEEN FOOD ALLERGY AND MATERNAL DIET
#######################################################################################################
NAvar1 <- list()
for (i in 1:ncol(AllergyMQ1)){
  if (sum(is.na(AllergyMQ1[,i]))>0){
    NAvar1 [[i]] <- which(is.na(AllergyMQ1[,i]))
  }
}
NAvar1[[which(colnames(AllergyMQ1)=='foodall_healthy')]]
NAvar1[[which(colnames(AllergyMQ1)=='birthseason')]]
NAvar1[[which(colnames(AllergyMQ1)=='anyher')]]
NAvar1[[which(colnames(AllergyMQ1)=='siblings')]]
NAvar1[[which(colnames(AllergyMQ1)=='Q1_Energykcal')]]

whremovefoodallPS1 <- unique(c(NAvar1[[which(colnames(AllergyMQ1)=='foodall_healthy')]],
                               NAvar1[[which(colnames(AllergyMQ1)=='birthseason')]],
                               NAvar1[[which(colnames(AllergyMQ1)=='anyher')]],
                               NAvar1[[which(colnames(AllergyMQ1)=='siblings')]],
                               NAvar1[[which(colnames(AllergyMQ1)=='Q1_Energykcal')]]))

foodallQ1NoNA <- AllergyMQ1[-whremovefoodallPS1,]

ConfoundersfoodallQ1NoNA <- cbind("Season of birth"=foodallQ1NoNA$birthseason, "Any heredity"=foodallQ1NoNA$anyher, "Siblings"=foodallQ1NoNA$siblings,
                                  "Energy intake"=foodallQ1NoNA$Q1_Energykcal)

foodallPartiellSpearmanQ1pvalue <- rep(0, times=ncol(MQ1))
foodallPartiellSpearmanQ1rho <- rep(0, times=ncol(MQ1))
for (i in 1:ncol(MQ1)) {
  foodallPartiellSpearmanQ1pvalue [i] <- pcor.test(foodallQ1NoNA$foodall_healthy, MQ1[-whremovefoodallPS1,i], ConfoundersfoodallQ1NoNA, method = "spearman")$p.value
  foodallPartiellSpearmanQ1rho [i] <- pcor.test(foodallQ1NoNA$foodall_healthy, MQ1[-whremovefoodallPS1,i], ConfoundersfoodallQ1NoNA, method = "spearman")$estimate
}

foodallPartiellSpearmanQ1pvalue
foodallPartiellSpearmanQ1rho

PartfoodallCorQ1fooditem <- (cbind(foodallPartiellSpearmanQ1pvalue,foodallPartiellSpearmanQ1rho))
rownames(PartfoodallCorQ1fooditem) <- colnames(MQ1)
PartfoodallCorQ1fooditem

#################################################################################
NAvar2 <- list()
for (i in 1:ncol(AllergyMQ2)){
  if (sum(is.na(AllergyMQ2[,i]))>0){
    NAvar2 [[i]] <- which(is.na(AllergyMQ2[,i]))
  }
}
NAvar2[[which(colnames(AllergyMQ2)=='foodall_healthy')]]
NAvar2[[which(colnames(AllergyMQ2)=='birthseason')]]
NAvar2[[which(colnames(AllergyMQ2)=='anyher')]]
NAvar2[[which(colnames(AllergyMQ2)=='siblings')]]
NAvar2[[which(colnames(AllergyMQ2)=='Q2_Energykcal')]]

whremovefoodallPS2 <- unique(c(NAvar2[[which(colnames(AllergyMQ2)=='foodall_healthy')]],
                               NAvar2[[which(colnames(AllergyMQ2)=='birthseason')]],
                               NAvar2[[which(colnames(AllergyMQ2)=='anyher')]],
                               NAvar2[[which(colnames(AllergyMQ2)=='siblings')]],
                               NAvar2[[which(colnames(AllergyMQ2)=='Q2_Energykcal')]]))

foodallQ2NoNA <- AllergyMQ2[-whremovefoodallPS2,]

ConfoundersfoodallQ2NoNA <- cbind("Season of birth"=foodallQ2NoNA$birthseason, "Any heredity"=foodallQ2NoNA$anyher, "Siblings"=foodallQ2NoNA$siblings,
                                  "Energy intake"=foodallQ2NoNA$Q2_Energykcal)

foodallPartiellSpearmanQ2pvalue <- rep(0, times=ncol(MQ2))
foodallPartiellSpearmanQ2rho <- rep(0, times=ncol(MQ2))
for (i in 1:ncol(MQ2)) {
  foodallPartiellSpearmanQ2pvalue [i] <- pcor.test(foodallQ2NoNA$foodall_healthy, MQ2[-whremovefoodallPS2,i], ConfoundersfoodallQ2NoNA, method = "spearman")$p.value
  foodallPartiellSpearmanQ2rho [i] <- pcor.test(foodallQ2NoNA$foodall_healthy, MQ2[-whremovefoodallPS2,i], ConfoundersfoodallQ2NoNA, method = "spearman")$estimate
}

foodallPartiellSpearmanQ2pvalue
foodallPartiellSpearmanQ2rho

PartfoodallCorQ2fooditem <- (cbind(foodallPartiellSpearmanQ2pvalue,foodallPartiellSpearmanQ2rho))
rownames(PartfoodallCorQ2fooditem) <- colnames(MQ2)
PartfoodallCorQ2fooditem

#################################################################################
NAvar3 <- list()
for (i in 1:ncol(AllergyMQ3)){
  if (sum(is.na(AllergyMQ3[,i]))>0){
    NAvar3 [[i]] <- which(is.na(AllergyMQ3[,i]))
  }
}
NAvar3[[which(colnames(AllergyMQ3)=='foodall_healthy')]]
NAvar3[[which(colnames(AllergyMQ3)=='birthseason')]]
NAvar3[[which(colnames(AllergyMQ3)=='anyher')]]
NAvar3[[which(colnames(AllergyMQ3)=='siblings')]]
NAvar3[[which(colnames(AllergyMQ3)=='Q3_Energykcal')]]

whremovefoodallPS3 <- unique(c(NAvar3[[which(colnames(AllergyMQ3)=='foodall_healthy')]],
                               NAvar3[[which(colnames(AllergyMQ3)=='birthseason')]],
                               NAvar3[[which(colnames(AllergyMQ3)=='anyher')]],
                               NAvar3[[which(colnames(AllergyMQ3)=='siblings')]],
                               NAvar3[[which(colnames(AllergyMQ3)=='Q3_Energykcal')]]))

foodallQ3NoNA <- AllergyMQ3[-whremovefoodallPS3,]

ConfoundersfoodallQ3NoNA <- cbind("Season of birth"=foodallQ3NoNA$birthseason, "Any heredity"=foodallQ3NoNA$anyher, "Siblings"=foodallQ3NoNA$siblings,
                                  "Energy intake"=foodallQ3NoNA$Q3_Energykcal)

foodallPartiellSpearmanQ3pvalue <- rep(0, times=ncol(MQ3))
foodallPartiellSpearmanQ3rho <- rep(0, times=ncol(MQ3))
for (i in 1:ncol(MQ3)) {
  foodallPartiellSpearmanQ3pvalue [i] <- pcor.test(foodallQ3NoNA$foodall_healthy, MQ3[-whremovefoodallPS3,i], ConfoundersfoodallQ3NoNA, method = "spearman")$p.value
  foodallPartiellSpearmanQ3rho [i] <- pcor.test(foodallQ3NoNA$foodall_healthy, MQ3[-whremovefoodallPS3,i], ConfoundersfoodallQ3NoNA, method = "spearman")$estimate
}

foodallPartiellSpearmanQ3pvalue
foodallPartiellSpearmanQ3rho

PartfoodallCorQ3fooditem <- (cbind(foodallPartiellSpearmanQ3pvalue,foodallPartiellSpearmanQ3rho))
rownames(PartfoodallCorQ3fooditem) <- colnames(MQ3)
PartfoodallCorQ3fooditem

#######################################################################################################
#CONFOUNDER-ADJUSTED SPEARMAN CORRELATIONS BETWEEN ATOPIC ECZEMA AND MATERNAL DIET
#######################################################################################################
NAvar1 <- list()
for (i in 1:ncol(AllergyMQ1)){
  if (sum(is.na(AllergyMQ1[,i]))>0){
    NAvar1 [[i]] <- which(is.na(AllergyMQ1[,i]))
  }
}
NAvar1[[which(colnames(AllergyMQ1)=='eczema_healthy')]]
NAvar1[[which(colnames(AllergyMQ1)=='birthseason')]]
NAvar1[[which(colnames(AllergyMQ1)=='anyher')]]
NAvar1[[which(colnames(AllergyMQ1)=='siblings')]]
NAvar1[[which(colnames(AllergyMQ1)=='Q1_Energykcal')]]

whremoveeczemaPS1 <- unique(c(NAvar1[[which(colnames(AllergyMQ1)=='eczema_healthy')]],
                              NAvar1[[which(colnames(AllergyMQ1)=='birthseason')]],
                              NAvar1[[which(colnames(AllergyMQ1)=='anyher')]],
                              NAvar1[[which(colnames(AllergyMQ1)=='siblings')]],
                              NAvar1[[which(colnames(AllergyMQ1)=='Q1_Energykcal')]]))

eczemaQ1NoNA <- AllergyMQ1[-whremoveeczemaPS1,]

ConfounderseczemaQ1NoNA <- cbind("Season of birth"=eczemaQ1NoNA$birthseason, "Any heredity"=eczemaQ1NoNA$anyher, "Siblings"=eczemaQ1NoNA$siblings,
                                 "Energy intake"=eczemaQ1NoNA$Q1_Energykcal)

eczemaPartiellSpearmanQ1pvalue <- rep(0, times=ncol(MQ1))
eczemaPartiellSpearmanQ1rho <- rep(0, times=ncol(MQ1))
for (i in 1:ncol(MQ1)) {
  eczemaPartiellSpearmanQ1pvalue [i] <- pcor.test(eczemaQ1NoNA$eczema_healthy, MQ1[-whremoveeczemaPS1,i], ConfounderseczemaQ1NoNA, method = "spearman")$p.value
  eczemaPartiellSpearmanQ1rho [i] <- pcor.test(eczemaQ1NoNA$eczema_healthy, MQ1[-whremoveeczemaPS1,i], ConfounderseczemaQ1NoNA, method = "spearman")$estimate
}

eczemaPartiellSpearmanQ1pvalue
eczemaPartiellSpearmanQ1rho

ParteczemaCorQ1fooditem <- (cbind(eczemaPartiellSpearmanQ1pvalue,eczemaPartiellSpearmanQ1rho))
rownames(ParteczemaCorQ1fooditem) <- colnames(MQ1)
ParteczemaCorQ1fooditem

#################################################################################
NAvar2 <- list()
for (i in 1:ncol(AllergyMQ2)){
  if (sum(is.na(AllergyMQ2[,i]))>0){
    NAvar2 [[i]] <- which(is.na(AllergyMQ2[,i]))
  }
}
NAvar2[[which(colnames(AllergyMQ2)=='eczema_healthy')]]
NAvar2[[which(colnames(AllergyMQ2)=='birthseason')]]
NAvar2[[which(colnames(AllergyMQ2)=='anyher')]]
NAvar2[[which(colnames(AllergyMQ2)=='siblings')]]
NAvar2[[which(colnames(AllergyMQ2)=='Q2_Energykcal')]]

whremoveeczemaPS2 <- unique(c(NAvar2[[which(colnames(AllergyMQ2)=='eczema_healthy')]],
                              NAvar2[[which(colnames(AllergyMQ2)=='birthseason')]],
                              NAvar2[[which(colnames(AllergyMQ2)=='anyher')]],
                              NAvar2[[which(colnames(AllergyMQ2)=='siblings')]],
                              NAvar2[[which(colnames(AllergyMQ2)=='Q2_Energykcal')]]))

eczemaQ2NoNA <- AllergyMQ2[-whremoveeczemaPS2,]

ConfounderseczemaQ2NoNA <- cbind("Season of birth"=eczemaQ2NoNA$birthseason, "Any heredity"=eczemaQ2NoNA$anyher, "Siblings"=eczemaQ2NoNA$siblings,
                                 "Energy intake"=eczemaQ2NoNA$Q2_Energykcal)

eczemaPartiellSpearmanQ2pvalue <- rep(0, times=ncol(MQ2))
eczemaPartiellSpearmanQ2rho <- rep(0, times=ncol(MQ2))
for (i in 1:ncol(MQ2)) {
  eczemaPartiellSpearmanQ2pvalue [i] <- pcor.test(eczemaQ2NoNA$eczema_healthy, MQ2[-whremoveeczemaPS2,i], ConfounderseczemaQ2NoNA, method = "spearman")$p.value
  eczemaPartiellSpearmanQ2rho [i] <- pcor.test(eczemaQ2NoNA$eczema_healthy, MQ2[-whremoveeczemaPS2,i], ConfounderseczemaQ2NoNA, method = "spearman")$estimate
}

eczemaPartiellSpearmanQ2pvalue
eczemaPartiellSpearmanQ2rho

ParteczemaCorQ2fooditem <- (cbind(eczemaPartiellSpearmanQ2pvalue,eczemaPartiellSpearmanQ2rho))
rownames(ParteczemaCorQ2fooditem) <- colnames(MQ2)
ParteczemaCorQ2fooditem

#################################################################################
NAvar3 <- list()
for (i in 1:ncol(AllergyMQ3)){
  if (sum(is.na(AllergyMQ3[,i]))>0){
    NAvar3 [[i]] <- which(is.na(AllergyMQ3[,i]))
  }
}
NAvar3[[which(colnames(AllergyMQ3)=='eczema_healthy')]]
NAvar3[[which(colnames(AllergyMQ3)=='birthseason')]]
NAvar3[[which(colnames(AllergyMQ3)=='anyher')]]
NAvar3[[which(colnames(AllergyMQ3)=='siblings')]]
NAvar3[[which(colnames(AllergyMQ3)=='Q3_Energykcal')]]

whremoveeczemaPS3 <- unique(c(NAvar3[[which(colnames(AllergyMQ3)=='eczema_healthy')]],
                              NAvar3[[which(colnames(AllergyMQ3)=='birthseason')]],
                              NAvar3[[which(colnames(AllergyMQ3)=='anyher')]],
                              NAvar3[[which(colnames(AllergyMQ3)=='siblings')]],
                              NAvar3[[which(colnames(AllergyMQ3)=='Q3_Energykcal')]]))

eczemaQ3NoNA <- AllergyMQ3[-whremoveeczemaPS3,]

ConfounderseczemaQ3NoNA <- cbind("Season of birth"=eczemaQ3NoNA$birthseason, "Any heredity"=eczemaQ3NoNA$anyher, "Siblings"=eczemaQ3NoNA$siblings,
                                 "Energy intake"=eczemaQ3NoNA$Q3_Energykcal)

eczemaPartiellSpearmanQ3pvalue <- rep(0, times=ncol(MQ3))
eczemaPartiellSpearmanQ3rho <- rep(0, times=ncol(MQ3))
for (i in 1:ncol(MQ3)) {
  eczemaPartiellSpearmanQ3pvalue [i] <- pcor.test(eczemaQ3NoNA$eczema_healthy, MQ3[-whremoveeczemaPS3,i], ConfounderseczemaQ3NoNA, method = "spearman")$p.value
  eczemaPartiellSpearmanQ3rho [i] <- pcor.test(eczemaQ3NoNA$eczema_healthy, MQ3[-whremoveeczemaPS3,i], ConfounderseczemaQ3NoNA, method = "spearman")$estimate
}

eczemaPartiellSpearmanQ3pvalue
eczemaPartiellSpearmanQ3rho

ParteczemaCorQ3fooditem <- (cbind(eczemaPartiellSpearmanQ3pvalue,eczemaPartiellSpearmanQ3rho))
rownames(ParteczemaCorQ3fooditem) <- colnames(MQ3)
ParteczemaCorQ3fooditem

#######################################################################################################
#CONFOUNDER-ADJUSTED SPEARMAN CORRELATIONS BETWEEN ASTHMA AND MATERNAL DIET
#######################################################################################################
NAvar1 <- list()
for (i in 1:ncol(AllergyMQ1)){
  if (sum(is.na(AllergyMQ1[,i]))>0){
    NAvar1 [[i]] <- which(is.na(AllergyMQ1[,i]))
  }
}
NAvar1[[which(colnames(AllergyMQ1)=='asthma_healthy')]]
NAvar1[[which(colnames(AllergyMQ1)=='birthseason')]]
NAvar1[[which(colnames(AllergyMQ1)=='anyher')]]
NAvar1[[which(colnames(AllergyMQ1)=='siblings')]]
NAvar1[[which(colnames(AllergyMQ1)=='Q1_Energykcal')]]

whremoveasthmaPS1 <- unique(c(NAvar1[[which(colnames(AllergyMQ1)=='asthma_healthy')]],
                              NAvar1[[which(colnames(AllergyMQ1)=='birthseason')]],
                              NAvar1[[which(colnames(AllergyMQ1)=='anyher')]],
                              NAvar1[[which(colnames(AllergyMQ1)=='siblings')]],
                              NAvar1[[which(colnames(AllergyMQ1)=='Q1_Energykcal')]]))

asthmaQ1NoNA <- AllergyMQ1[-whremoveasthmaPS1,]

ConfoundersasthmaQ1NoNA <- cbind("Season of birth"=asthmaQ1NoNA$birthseason, "Any heredity"=asthmaQ1NoNA$anyher, "Siblings"=asthmaQ1NoNA$siblings,
                                 "Energy intake"=asthmaQ1NoNA$Q1_Energykcal)

asthmaPartiellSpearmanQ1pvalue <- rep(0, times=ncol(MQ1))
asthmaPartiellSpearmanQ1rho <- rep(0, times=ncol(MQ1))
for (i in 1:ncol(MQ1)) {
  asthmaPartiellSpearmanQ1pvalue [i] <- pcor.test(asthmaQ1NoNA$asthma_healthy, MQ1[-whremoveasthmaPS1,i], ConfoundersasthmaQ1NoNA, method = "spearman")$p.value
  asthmaPartiellSpearmanQ1rho [i] <- pcor.test(asthmaQ1NoNA$asthma_healthy, MQ1[-whremoveasthmaPS1,i], ConfoundersasthmaQ1NoNA, method = "spearman")$estimate
}

asthmaPartiellSpearmanQ1pvalue
asthmaPartiellSpearmanQ1rho

PartasthmaCorQ1fooditem <- (cbind(asthmaPartiellSpearmanQ1pvalue,asthmaPartiellSpearmanQ1rho))
rownames(PartasthmaCorQ1fooditem) <- colnames(MQ1)
PartasthmaCorQ1fooditem

#################################################################################
NAvar2 <- list()
for (i in 1:ncol(AllergyMQ2)){
  if (sum(is.na(AllergyMQ2[,i]))>0){
    NAvar2 [[i]] <- which(is.na(AllergyMQ2[,i]))
  }
}
NAvar2[[which(colnames(AllergyMQ2)=='asthma_healthy')]]
NAvar2[[which(colnames(AllergyMQ2)=='birthseason')]]
NAvar2[[which(colnames(AllergyMQ2)=='anyher')]]
NAvar2[[which(colnames(AllergyMQ2)=='siblings')]]
NAvar2[[which(colnames(AllergyMQ2)=='Q2_Energykcal')]]

whremoveasthmaPS2 <- unique(c(NAvar2[[which(colnames(AllergyMQ2)=='asthma_healthy')]],
                              NAvar2[[which(colnames(AllergyMQ2)=='birthseason')]],
                              NAvar2[[which(colnames(AllergyMQ2)=='anyher')]],
                              NAvar2[[which(colnames(AllergyMQ2)=='siblings')]],
                              NAvar2[[which(colnames(AllergyMQ2)=='Q2_Energykcal')]]))

asthmaQ2NoNA <- AllergyMQ2[-whremoveasthmaPS2,]

ConfoundersasthmaQ2NoNA <- cbind("Season of birth"=asthmaQ2NoNA$birthseason, "Any heredity"=asthmaQ2NoNA$anyher, "Siblings"=asthmaQ2NoNA$siblings,
                                 "Energy intake"=asthmaQ2NoNA$Q2_Energykcal)

asthmaPartiellSpearmanQ2pvalue <- rep(0, times=ncol(MQ2))
asthmaPartiellSpearmanQ2rho <- rep(0, times=ncol(MQ2))
for (i in 1:ncol(MQ2)) {
  asthmaPartiellSpearmanQ2pvalue [i] <- pcor.test(asthmaQ2NoNA$asthma_healthy, MQ2[-whremoveasthmaPS2,i], ConfoundersasthmaQ2NoNA, method = "spearman")$p.value
  asthmaPartiellSpearmanQ2rho [i] <- pcor.test(asthmaQ2NoNA$asthma_healthy, MQ2[-whremoveasthmaPS2,i], ConfoundersasthmaQ2NoNA, method = "spearman")$estimate
}

asthmaPartiellSpearmanQ2pvalue
asthmaPartiellSpearmanQ2rho

PartasthmaCorQ2fooditem <- (cbind(asthmaPartiellSpearmanQ2pvalue,asthmaPartiellSpearmanQ2rho))
rownames(PartasthmaCorQ2fooditem) <- colnames(MQ2)
PartasthmaCorQ2fooditem

#################################################################################
NAvar3 <- list()
for (i in 1:ncol(AllergyMQ3)){
  if (sum(is.na(AllergyMQ3[,i]))>0){
    NAvar3 [[i]] <- which(is.na(AllergyMQ3[,i]))
  }
}
NAvar3[[which(colnames(AllergyMQ3)=='asthma_healthy')]]
NAvar3[[which(colnames(AllergyMQ3)=='birthseason')]]
NAvar3[[which(colnames(AllergyMQ3)=='anyher')]]
NAvar3[[which(colnames(AllergyMQ3)=='siblings')]]
NAvar3[[which(colnames(AllergyMQ3)=='Q3_Energykcal')]]

whremoveasthmaPS3 <- unique(c(NAvar3[[which(colnames(AllergyMQ3)=='asthma_healthy')]],
                              NAvar3[[which(colnames(AllergyMQ3)=='birthseason')]],
                              NAvar3[[which(colnames(AllergyMQ3)=='anyher')]],
                              NAvar3[[which(colnames(AllergyMQ3)=='siblings')]],
                              NAvar3[[which(colnames(AllergyMQ3)=='Q3_Energykcal')]]))

asthmaQ3NoNA <- AllergyMQ3[-whremoveasthmaPS3,]

ConfoundersasthmaQ3NoNA <- cbind("Season of birth"=asthmaQ3NoNA$birthseason, "Any heredity"=asthmaQ3NoNA$anyher, "Siblings"=asthmaQ3NoNA$siblings,
                                 "Energy intake"=asthmaQ3NoNA$Q3_Energykcal)

asthmaPartiellSpearmanQ3pvalue <- rep(0, times=ncol(MQ3))
asthmaPartiellSpearmanQ3rho <- rep(0, times=ncol(MQ3))
for (i in 1:ncol(MQ3)) {
  asthmaPartiellSpearmanQ3pvalue [i] <- pcor.test(asthmaQ3NoNA$asthma_healthy, MQ3[-whremoveasthmaPS3,i], ConfoundersasthmaQ3NoNA, method = "spearman")$p.value
  asthmaPartiellSpearmanQ3rho [i] <- pcor.test(asthmaQ3NoNA$asthma_healthy, MQ3[-whremoveasthmaPS3,i], ConfoundersasthmaQ3NoNA, method = "spearman")$estimate
}

asthmaPartiellSpearmanQ3pvalue
asthmaPartiellSpearmanQ3rho

PartasthmaCorQ3fooditem <- (cbind(asthmaPartiellSpearmanQ3pvalue,asthmaPartiellSpearmanQ3rho))
rownames(PartasthmaCorQ3fooditem) <- colnames(MQ3)
PartasthmaCorQ3fooditem

#######################################################################################################
#VISUALIZATION WITH CLUSTERED HEATMAPS
#######################################################################################################
FAPartCorQ123fooditem <- (cbind("Pregnancy"=foodallPartiellSpearmanQ1rho,
                                "Lactation, 1 month"=foodallPartiellSpearmanQ2rho, 
                                "Lactation, 4 months"=foodallPartiellSpearmanQ3rho))

EczPartCorQ123fooditem <- (cbind("Pregnancy"=eczemaPartiellSpearmanQ1rho,
                                 "Lactation, 1 month"=eczemaPartiellSpearmanQ2rho, 
                                 "Lactation, 4 months"=eczemaPartiellSpearmanQ3rho))

AstPartCorQ123fooditem <- (cbind("Pregnancy"=asthmaPartiellSpearmanQ1rho,
                                 "Lactation, 1 month"=asthmaPartiellSpearmanQ2rho, 
                                 "Lactation, 4 months"=asthmaPartiellSpearmanQ3rho))

rownames(FAPartCorQ123fooditem) <- colnames(MQ1)
z1FA<-FAPartCorQ123fooditem

rownames(EczPartCorQ123fooditem) <- colnames(MQ1)
z1Ecz<-EczPartCorQ123fooditem

rownames(AstPartCorQ123fooditem) <- colnames(MQ1)
z1Ast<-AstPartCorQ123fooditem

library(pheatmap)

pdf("Q123 Food items Longitudinell Heatmaps (All confounders+kcal) 201015.pdf", width=14, height=7)
pheatmap(t(z1FA), cluster_rows = FALSE, main = "Food allergy", fontsize = 14, cellwidth=17, angle_col = "45", breaks = seq(from=-0.2, to=0.2, length.out = 321), color = colorRampPalette(c("lightblue", "white","brown1"))(320))
pheatmap(t(z1Ecz), cluster_rows = FALSE, main = "Atopic eczema", fontsize = 14, cellwidth=17, angle_col = "45", breaks = seq(from=-0.2, to=0.2, length.out = 321), color = colorRampPalette(c("lightblue", "white","brown1"))(320))
pheatmap(t(z1Ast), cluster_rows = FALSE, main = "Asthma", fontsize = 14, cellwidth=17, angle_col = "45", breaks = seq(from=-0.2, to=0.2, length.out = 321), color = colorRampPalette(c("lightblue", "white","brown1"))(320))
dev.off()
